import { resolve } from 'path';
import { defineConfig } from "vite";
import createReScriptPlugin from '@jihchi/vite-plugin-rescript';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [createReScriptPlugin({
    loader: {
      output: './lib/js',
      suffix: 'res.js',
    },
    silent: false,
  }),],
  build: {
    entry: 'src/Main.res',
    lib: {
      entry: resolve(__dirname, 'src/GeoMap/GeoMap.res.mjs'),
      name: 'GeoMap',
      // the proper extensions will be added
      fileName: 'GeoMap',
    },
  },
});
