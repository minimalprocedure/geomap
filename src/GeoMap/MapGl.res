/* Copyright (c) 2024 Massimo Ghisalberti
*
*  This software is released under the MIT License.
*  https://opensource.org/licenses/MIT 
*/

type _V3OrV4Protocol

//type layer_paint = {@as("line-color") line_color: string}

//type layer_metadata = {}

@spice
type layer = {
  id: string,
  @as("type") type_: string,
  //metadata?: layer_metadata,
  source?: string,
  @as("source-layer") source_layer?: string,
  minzoom?: float,
  maxzoom?: float,
  //filter?: layer_filter_expression,
  //paint: layer_paint,
}

@spice
type layers = array<layer>

@spice
type bounds = list<float>

@spice
type protomaps = {
  @as("type") type_: string,
  url: string,
  attribution: string,
}

@spice
type sources = {protomaps: protomaps}

@spice
type style = {
  version: int,
  glyphs: string,
  sprite: string,
  sources: sources,
  layers?: layers,
}

@spice
type mapOption = {
  container: string,
  style: style,
  center?: LngLat.lngLat,
  bounds?: bounds,
  zoom?: float,
}

module Source = {
  type t = {bounds: bounds}
  @module("pmtiles") @scope("Source") @val external getKey: unit => string = "getKey"
}

@module("maplibre-gl") @val external addProtocol: (string, _V3OrV4Protocol) => unit = "addProtocol"

module Protocol = {
  type t = {tile: _V3OrV4Protocol}
  @module("pmtiles") @scope("Protocol") @val external tile: unit => _V3OrV4Protocol = "tile"
  @module("pmtiles") @new external make: unit => t = "Protocol"

  let init = () => {
    let protocol = make()
    addProtocol("pmtiles", protocol.tile)
  }
}

type mapGl = {
  on: (string, unit => unit) => unit,
  setMaxBounds: bounds => unit,
  getSource: string => Source.t,
}

@module("maplibre-gl") external maplibre: unit => unit = "maplibre"
@module("pmtiles") external pmtiles: unit = "pmtiles"

@module("protomaps-themes-base") external layers: (string, string) => layers = "default"

@module("maplibre-gl") @new external _make: mapOption => mapGl = "Map"

let make = mapOption => {
  let map = _make(mapOption)
  Protocol.init()
  map
}

@send external getSource: string => bounds = "getSource"
@send external setMaxBounds: bounds => unit = "setMaxBounds"
@send external on: (string, unit => unit) => unit = "on"
