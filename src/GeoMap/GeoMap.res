/* Copyright (c) 2024 Massimo Ghisalberti
 *
 *  This software is released under the MIT License.
 *  https://opensource.org/licenses/MIT
 */

open! Webapi.Dom
open MapGl

@spice
type markerCaption =
  MarkerCaption(string) | MarkerIcon(string, string, string) | MarkerImage(string, string)

module MarkerOptions = {
  @spice
  type t = {
    caption: markerCaption,
    subCaption: option<string>,
    text: string,
    coord: LngLat.lngLat,
    dir?: bool,
  }
  @spice
  type markers = array<t>
}

module Net = {
  @val @scope(("window", "location")) external protocol: string = "protocol"
  @val @scope(("window", "location")) external host: string = "host"
  @val @scope(("window", "location")) external pathname: string = "pathname"
}

module GeoMapOptions = {
  @spice
  type t = {
    container: string,
    center: LngLat.lngLat,
    zoom: float,
    fontsRoot: string,
    spriteRoot: string,
    pmtiles: string,
    attributions: array<string>,
    theme: string,
  }

  let make = options => {
    let protRex = RegExp.fromString("^https?:\\/\\/")
    let pmtiles = if RegExp.test(protRex, options.pmtiles) {
      `pmtiles://${options.pmtiles}`
    } else {
      let protHost = `${Net.protocol}//${Net.host}`
      `pmtiles://${protHost}${Net.pathname}${options.pmtiles}`
    }
    {
      container: options.container,
      center: options.center,
      zoom: options.zoom,
      style: {
        version: 8,
        glyphs: `${options.fontsRoot}/{fontstack}/{range}.pbf`,
        sprite: `${options.spriteRoot}/${options.theme}`,
        sources: {
          protomaps: {
            type_: "vector",
            url: pmtiles,
            attribution: Array.join(options.attributions, " | "),
          },
        },
      },
    }
  }
}

let makeMarker = (options: MarkerOptions.t) => {
  let marker = document->Document.createElement("div")
  marker->Element.setAttribute("class", "geo-marker")

  let geoIcon = document->Document.createElement("div")
  geoIcon->Element.setAttribute("class", "geo-icon")

  let geoInfo = document->Document.createElement("div")
  geoInfo->Element.setAttribute("class", "geo-info pop-box")

  let popInfo = document->Document.createElement("div")
  popInfo->Element.setAttribute("class", "pop-info")

  marker->Element.appendChild(~child=geoIcon)
  marker->Element.appendChild(~child=geoInfo)
  geoInfo->Element.appendChild(~child=popInfo)

  switch options.dir {
  | None => ()
  | Some(show) =>
    if show {
      let popDir = document->Document.createElement("i")
      popDir->Element.setAttribute("class", "pop-dir top pos-top-left")
      geoInfo->Element.appendChild(~child=popDir)
    } else {
      ()
    }
  }

  let markerCaption = switch options.caption {
  | MarkerCaption(caption) =>
    let title = document->Document.createElement("div")
    title->Element.setAttribute("class", "geo-icon-caption")
    title->Element.setInnerHTML(caption)
    geoIcon->Element.appendChild(~child=title)
    title
  | MarkerIcon(iconClass, color, title) => {
      let icon = document->Document.createElement("i")
      icon->Element.setAttribute("class", `geo-icon-icon nf ${iconClass}`)
      icon->Element.setAttribute("style", `color:${color}`)
      icon->Element.setAttribute("title", title)
      geoIcon->Element.appendChild(~child=icon)
      icon
    }
  | MarkerImage(imgPath, title) => {
      let img = document->Document.createElement("img")
      img->Element.setAttribute("class", "geo-icon-image")
      img->Element.setAttribute("alt", title)
      img->Element.setAttribute("title", title)
      img->Element.setAttribute("src", imgPath)
      geoIcon->Element.appendChild(~child=img)
      img
    }
  }
  switch options.subCaption {
  | None => ()
  | Some(sub) => {
      let subElt = document->Document.createElement("div")
      subElt->Element.setAttribute("class", "geo-icon-subcaption")
      subElt->Element.setInnerHTML(sub)
      geoIcon->Element.appendChild(~child=subElt)
    }
  }
  popInfo->Element.setInnerHTML(options.text)

  let dset = (v, top, left) =>
    `display:${v};position:absolute;top:${Int.toString(top)}px;left:${Int.toString(left)}px`

  let showHide = (elt, type_, x, y) => {
    let hover = (elt, display, action) =>
      switch Element.getAttribute(elt, "data-status") {
      | None => {
          Element.setAttribute(elt, "data-status", "")
          Element.setAttribute(elt, "style", dset(display, x, y))
        }
      | Some(s) =>
        switch s {
        | "clicked" =>
          if action == "click" {
            Element.setAttribute(elt, "data-status", "")
            Element.setAttribute(elt, "style", dset("none", x, y))
          } else {
            ()
          }
        | _ =>
          if action == "click" {
            Element.setAttribute(elt, "data-status", "clicked")
            Element.setAttribute(elt, "style", dset(display, x, y))
          } else {
            Element.setAttribute(elt, "data-status", "")
            Element.setAttribute(elt, "style", dset(display, x, y))
          }
        }
      }
    switch type_ {
    | "mousedown" => hover(elt, "block", "click")
    | "mouseenter" => hover(elt, "block", "")
    | "mouseout" => hover(elt, "none", "")
    | _ => {
        Element.setAttribute(elt, "data-status", "")
        Element.setAttribute(elt, "style", "display:none")
      }
    }
  }

  let popup = ev => {
    MouseEvent.stopImmediatePropagation(ev)

    let typ = MouseEvent.type_(ev)
    let target = ev->MouseEvent.target->EventTarget.unsafeAsElement
    let parent = Element.parentElement(target)

    switch parent {
    | None => ()
    | Some(parent) => {
        let targetSibling = Element.nextElementSibling(parent)
        switch targetSibling {
        | None => ()
        | Some(elt) => {
            let bounds = Element.getBoundingClientRect(parent)
            let ptop = DomRect.height(bounds)->Int.fromFloat
            let dir =
              (elt->Element.getElementsByClassName("pos-top-left")->HtmlCollection.toArray)[0]
            switch dir {
            | None => showHide(elt, typ, ptop + 4, 0 - ptop / 2)
            | Some(dir) => {
                let cstyle = Window.getComputedStyle(window, dir)
                let left = CssStyleDeclaration.left(cstyle)->Core__Int.fromString->Option.getOr(0)
                /* FIXME: accrocco! */
                showHide(elt, typ, ptop * 2 + 4, -left - ptop / 2)
              }
            }
          }
        }
      }
    }
  }
  Element.addMouseOutEventListener(markerCaption, popup)
  Element.addMouseEnterEventListener(markerCaption, popup)
  Element.addMouseDownEventListener(geoIcon, popup)

  marker
}

module MakeMapOptions = {
  type t = JSON(Js.Json.t) | Struct(GeoMapOptions.t)
}

module MakeMarkerOptions = {
  type t = JSON(Js.Json.t) | Struct(array<MarkerOptions.t>)
}

let addMarker = (map, markerOptions) => {
  let markerEl = makeMarker(markerOptions)
  let marker = Marker.make({element: markerEl}).setLngLat(markerOptions.coord).addTo(map)
  marker
}

let addMarkers = (map, markers) => {
  Array.reduce(markers, [], (acc, m) => {
    Array.push(acc, addMarker(map, m))
    acc
  })
}

let addMarkersJson = (map, json) => {
  let markers = MarkerOptions.markers_decode(json)
  switch markers {
  | Error(_) => []
  | Ok(markers) => addMarkers(map, markers)
  }
}

let makeMapStruct = options => {
  let mapOpts = GeoMapOptions.make(options)
  let style = {...mapOpts.style, layers: layers("protomaps", options.theme)}
  let mapOpts = {...mapOpts, style}
  let map = MapGl.make(mapOpts)
  map.on("load", () => {
    let myBounds = map.getSource("protomaps").bounds
    map.setMaxBounds(myBounds)
  })
  Some(map)
}

let makeMapJson = json => {
  switch GeoMapOptions.t_decode(json) {
  | Error(_) => None
  | Ok(opts) => makeMapStruct(opts)
  }
}

let makeMap = options => {
  switch options {
  | MakeMapOptions.JSON(json) => makeMapJson(json)
  | MakeMapOptions.Struct(options) => makeMapStruct(options)
  }
}

let makeMapWithMarkers = (options, markers) => {
  let map = makeMapStruct(options)
  switch map {
  | None => (None, [])
  | Some(map) => (Some(map), addMarkers(map, markers))
  }
}

let makeMapWithMarkersJson = (options, markers) => {
  let map = makeMapJson(options)
  switch map {
  | None => (None, [])
  | Some(map) => (Some(map), addMarkersJson(map, markers))
  }
}

let makeMapWithMarkers = (options, markers) => {
  switch (options, markers) {
  | (MakeMapOptions.JSON(mapOpts), MakeMarkerOptions.JSON(markOpts)) =>
    makeMapWithMarkersJson(mapOpts, markOpts)
  | (MakeMapOptions.JSON(mapOpts), MakeMarkerOptions.Struct(markOpts)) =>
    switch makeMapJson(mapOpts) {
    | None => (None, [])
    | Some(map) => (Some(map), addMarkers(map, markOpts))
    }
  | (MakeMapOptions.Struct(mapOpts), MakeMarkerOptions.Struct(markOpts)) =>
    makeMapWithMarkers(mapOpts, markOpts)
  | (MakeMapOptions.Struct(mapOpts), MakeMarkerOptions.JSON(markOpts)) =>
    switch makeMapStruct(mapOpts) {
    | None => (None, [])
    | Some(map) => (Some(map), addMarkersJson(map, markOpts))
    }
  }
}
