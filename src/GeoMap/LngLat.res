/* Copyright (c) 2024 Massimo Ghisalberti
*
*  This software is released under the MIT License.
*  https://opensource.org/licenses/MIT 
*/

type lngLatStruct = {
  lon: float,
  lat: float,
}

@spice
type lngLat = array<float>

module Cardinal = {
  type t = N | E | S | W
}

let degToDec = (d, m, s, c) => {
  let dec = d +. m /. 60.0 +. s /. 3600.0
  switch c {
  | Cardinal.N => dec
  | Cardinal.E => dec
  | Cardinal.S => -.dec
  | Cardinal.W => -.dec
  }
}
