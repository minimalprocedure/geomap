// Generated by ReScript, PLEASE EDIT WITH CARE

import * as MapGl from "./MapGl.res.mjs";
import * as Spice from "@greenlabs/ppx-spice/src/rescript/Spice.res.mjs";
import * as LngLat from "./LngLat.res.mjs";
import * as Js_dict from "rescript/lib/es6/js_dict.js";
import * as Caml_obj from "rescript/lib/es6/caml_obj.js";
import * as Core__Int from "@rescript/core/src/Core__Int.res.mjs";
import * as Belt_Array from "rescript/lib/es6/belt_Array.js";
import * as Belt_Option from "rescript/lib/es6/belt_Option.js";
import * as Caml_option from "rescript/lib/es6/caml_option.js";
import * as Core__Array from "@rescript/core/src/Core__Array.res.mjs";
import * as MaplibreGl from "maplibre-gl";
import * as Core__Option from "@rescript/core/src/Core__Option.res.mjs";
import ProtomapsThemesBase from "protomaps-themes-base";

function markerCaption_encode(v) {
  switch (v.TAG) {
    case "MarkerCaption" :
        return [
                "MarkerCaption",
                Spice.stringToJson(v._0)
              ];
    case "MarkerIcon" :
        return [
                "MarkerIcon",
                Spice.stringToJson(v._0),
                Spice.stringToJson(v._1),
                Spice.stringToJson(v._2)
              ];
    case "MarkerImage" :
        return [
                "MarkerImage",
                Spice.stringToJson(v._0),
                Spice.stringToJson(v._1)
              ];
    
  }
}

function markerCaption_decode(v) {
  if (!Array.isArray(v) && (v === null || typeof v !== "object") && typeof v !== "number" && typeof v !== "string" && typeof v !== "boolean") {
    return Spice.error(undefined, "Not a variant", v);
  }
  if (!Array.isArray(v)) {
    return Spice.error(undefined, "Not a variant", v);
  }
  if (v.length === 0) {
    return Spice.error(undefined, "Expected variant, found empty array", v);
  }
  var match = Belt_Array.getExn(v, 0);
  if (!(!Array.isArray(match) && (match === null || typeof match !== "object") && typeof match !== "number" && typeof match !== "string" && typeof match !== "boolean") && typeof match === "string") {
    switch (match) {
      case "MarkerCaption" :
          if (v.length !== 2) {
            return Spice.error(undefined, "Invalid number of arguments to variant constructor", v);
          }
          var v0 = Spice.stringFromJson(Belt_Array.getExn(v, 1));
          if (v0.TAG === "Ok") {
            return {
                    TAG: "Ok",
                    _0: {
                      TAG: "MarkerCaption",
                      _0: v0._0
                    }
                  };
          }
          var e = v0._0;
          return {
                  TAG: "Error",
                  _0: {
                    path: "[0]" + e.path,
                    message: e.message,
                    value: e.value
                  }
                };
      case "MarkerIcon" :
          if (v.length !== 4) {
            return Spice.error(undefined, "Invalid number of arguments to variant constructor", v);
          }
          var match$1 = Spice.stringFromJson(Belt_Array.getExn(v, 1));
          var match$2 = Spice.stringFromJson(Belt_Array.getExn(v, 2));
          var match$3 = Spice.stringFromJson(Belt_Array.getExn(v, 3));
          if (match$1.TAG === "Ok") {
            if (match$2.TAG === "Ok") {
              if (match$3.TAG === "Ok") {
                return {
                        TAG: "Ok",
                        _0: {
                          TAG: "MarkerIcon",
                          _0: match$1._0,
                          _1: match$2._0,
                          _2: match$3._0
                        }
                      };
              }
              var e$1 = match$3._0;
              return {
                      TAG: "Error",
                      _0: {
                        path: "[2]" + e$1.path,
                        message: e$1.message,
                        value: e$1.value
                      }
                    };
            }
            var e$2 = match$2._0;
            return {
                    TAG: "Error",
                    _0: {
                      path: "[1]" + e$2.path,
                      message: e$2.message,
                      value: e$2.value
                    }
                  };
          }
          var e$3 = match$1._0;
          return {
                  TAG: "Error",
                  _0: {
                    path: "[0]" + e$3.path,
                    message: e$3.message,
                    value: e$3.value
                  }
                };
      case "MarkerImage" :
          if (v.length !== 3) {
            return Spice.error(undefined, "Invalid number of arguments to variant constructor", v);
          }
          var match$4 = Spice.stringFromJson(Belt_Array.getExn(v, 1));
          var match$5 = Spice.stringFromJson(Belt_Array.getExn(v, 2));
          if (match$4.TAG === "Ok") {
            if (match$5.TAG === "Ok") {
              return {
                      TAG: "Ok",
                      _0: {
                        TAG: "MarkerImage",
                        _0: match$4._0,
                        _1: match$5._0
                      }
                    };
            }
            var e$4 = match$5._0;
            return {
                    TAG: "Error",
                    _0: {
                      path: "[1]" + e$4.path,
                      message: e$4.message,
                      value: e$4.value
                    }
                  };
          }
          var e$5 = match$4._0;
          return {
                  TAG: "Error",
                  _0: {
                    path: "[0]" + e$5.path,
                    message: e$5.message,
                    value: e$5.value
                  }
                };
      default:
        
    }
  }
  return Spice.error(undefined, "Invalid variant constructor", Belt_Array.getExn(v, 0));
}

function t_encode(v) {
  return Js_dict.fromArray(Spice.filterOptional([
                  [
                    "caption",
                    false,
                    markerCaption_encode(v.caption)
                  ],
                  [
                    "subCaption",
                    false,
                    (function (extra) {
                          return Spice.optionToJson(Spice.stringToJson, extra);
                        })(v.subCaption)
                  ],
                  [
                    "text",
                    false,
                    Spice.stringToJson(v.text)
                  ],
                  [
                    "coord",
                    false,
                    LngLat.lngLat_encode(v.coord)
                  ],
                  [
                    "dir",
                    true,
                    (function (extra) {
                          return Spice.optionToJson(Spice.boolToJson, extra);
                        })(v.dir)
                  ]
                ]));
}

function t_decode(v) {
  if (!Array.isArray(v) && (v === null || typeof v !== "object") && typeof v !== "number" && typeof v !== "string" && typeof v !== "boolean") {
    return Spice.error(undefined, "Not an object", v);
  }
  if (!(typeof v === "object" && !Array.isArray(v))) {
    return Spice.error(undefined, "Not an object", v);
  }
  var caption = markerCaption_decode(Belt_Option.getWithDefault(Js_dict.get(v, "caption"), null));
  if (caption.TAG === "Ok") {
    var subCaption = (function (extra) {
          return Spice.optionFromJson(Spice.stringFromJson, extra);
        })(Belt_Option.getWithDefault(Js_dict.get(v, "subCaption"), null));
    if (subCaption.TAG === "Ok") {
      var text = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "text"), null));
      if (text.TAG === "Ok") {
        var coord = LngLat.lngLat_decode(Belt_Option.getWithDefault(Js_dict.get(v, "coord"), null));
        if (coord.TAG === "Ok") {
          var dir = (function (extra) {
                return Spice.optionFromJson(Spice.boolFromJson, extra);
              })(Belt_Option.getWithDefault(Js_dict.get(v, "dir"), null));
          if (dir.TAG === "Ok") {
            return {
                    TAG: "Ok",
                    _0: {
                      caption: caption._0,
                      subCaption: subCaption._0,
                      text: text._0,
                      coord: coord._0,
                      dir: dir._0
                    }
                  };
          }
          var e = dir._0;
          return {
                  TAG: "Error",
                  _0: {
                    path: ".dir" + e.path,
                    message: e.message,
                    value: e.value
                  }
                };
        }
        var e$1 = coord._0;
        return {
                TAG: "Error",
                _0: {
                  path: ".coord" + e$1.path,
                  message: e$1.message,
                  value: e$1.value
                }
              };
      }
      var e$2 = text._0;
      return {
              TAG: "Error",
              _0: {
                path: ".text" + e$2.path,
                message: e$2.message,
                value: e$2.value
              }
            };
    }
    var e$3 = subCaption._0;
    return {
            TAG: "Error",
            _0: {
              path: ".subCaption" + e$3.path,
              message: e$3.message,
              value: e$3.value
            }
          };
  }
  var e$4 = caption._0;
  return {
          TAG: "Error",
          _0: {
            path: ".caption" + e$4.path,
            message: e$4.message,
            value: e$4.value
          }
        };
}

function markers_encode(v) {
  return Spice.arrayToJson(t_encode, v);
}

function markers_decode(v) {
  return Spice.arrayFromJson(t_decode, v);
}

var MarkerOptions = {
  t_encode: t_encode,
  t_decode: t_decode,
  markers_encode: markers_encode,
  markers_decode: markers_decode
};

var Net = {};

function t_encode$1(v) {
  return Js_dict.fromArray(Spice.filterOptional([
                  [
                    "container",
                    false,
                    Spice.stringToJson(v.container)
                  ],
                  [
                    "center",
                    false,
                    LngLat.lngLat_encode(v.center)
                  ],
                  [
                    "zoom",
                    false,
                    Spice.floatToJson(v.zoom)
                  ],
                  [
                    "fontsRoot",
                    false,
                    Spice.stringToJson(v.fontsRoot)
                  ],
                  [
                    "spriteRoot",
                    false,
                    Spice.stringToJson(v.spriteRoot)
                  ],
                  [
                    "pmtiles",
                    false,
                    Spice.stringToJson(v.pmtiles)
                  ],
                  [
                    "attributions",
                    false,
                    (function (extra) {
                          return Spice.arrayToJson(Spice.stringToJson, extra);
                        })(v.attributions)
                  ],
                  [
                    "theme",
                    false,
                    Spice.stringToJson(v.theme)
                  ]
                ]));
}

function t_decode$1(v) {
  if (!Array.isArray(v) && (v === null || typeof v !== "object") && typeof v !== "number" && typeof v !== "string" && typeof v !== "boolean") {
    return Spice.error(undefined, "Not an object", v);
  }
  if (!(typeof v === "object" && !Array.isArray(v))) {
    return Spice.error(undefined, "Not an object", v);
  }
  var container = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "container"), null));
  if (container.TAG === "Ok") {
    var center = LngLat.lngLat_decode(Belt_Option.getWithDefault(Js_dict.get(v, "center"), null));
    if (center.TAG === "Ok") {
      var zoom = Spice.floatFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "zoom"), null));
      if (zoom.TAG === "Ok") {
        var fontsRoot = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "fontsRoot"), null));
        if (fontsRoot.TAG === "Ok") {
          var spriteRoot = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "spriteRoot"), null));
          if (spriteRoot.TAG === "Ok") {
            var pmtiles = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "pmtiles"), null));
            if (pmtiles.TAG === "Ok") {
              var attributions = (function (extra) {
                    return Spice.arrayFromJson(Spice.stringFromJson, extra);
                  })(Belt_Option.getWithDefault(Js_dict.get(v, "attributions"), null));
              if (attributions.TAG === "Ok") {
                var theme = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "theme"), null));
                if (theme.TAG === "Ok") {
                  return {
                          TAG: "Ok",
                          _0: {
                            container: container._0,
                            center: center._0,
                            zoom: zoom._0,
                            fontsRoot: fontsRoot._0,
                            spriteRoot: spriteRoot._0,
                            pmtiles: pmtiles._0,
                            attributions: attributions._0,
                            theme: theme._0
                          }
                        };
                }
                var e = theme._0;
                return {
                        TAG: "Error",
                        _0: {
                          path: ".theme" + e.path,
                          message: e.message,
                          value: e.value
                        }
                      };
              }
              var e$1 = attributions._0;
              return {
                      TAG: "Error",
                      _0: {
                        path: ".attributions" + e$1.path,
                        message: e$1.message,
                        value: e$1.value
                      }
                    };
            }
            var e$2 = pmtiles._0;
            return {
                    TAG: "Error",
                    _0: {
                      path: ".pmtiles" + e$2.path,
                      message: e$2.message,
                      value: e$2.value
                    }
                  };
          }
          var e$3 = spriteRoot._0;
          return {
                  TAG: "Error",
                  _0: {
                    path: ".spriteRoot" + e$3.path,
                    message: e$3.message,
                    value: e$3.value
                  }
                };
        }
        var e$4 = fontsRoot._0;
        return {
                TAG: "Error",
                _0: {
                  path: ".fontsRoot" + e$4.path,
                  message: e$4.message,
                  value: e$4.value
                }
              };
      }
      var e$5 = zoom._0;
      return {
              TAG: "Error",
              _0: {
                path: ".zoom" + e$5.path,
                message: e$5.message,
                value: e$5.value
              }
            };
    }
    var e$6 = center._0;
    return {
            TAG: "Error",
            _0: {
              path: ".center" + e$6.path,
              message: e$6.message,
              value: e$6.value
            }
          };
  }
  var e$7 = container._0;
  return {
          TAG: "Error",
          _0: {
            path: ".container" + e$7.path,
            message: e$7.message,
            value: e$7.value
          }
        };
}

function make(options) {
  var protRex = new RegExp("^https?:\\/\\/");
  var pmtiles;
  if (protRex.test(options.pmtiles)) {
    pmtiles = "pmtiles://" + options.pmtiles;
  } else {
    var protHost = window.location.protocol + "//" + window.location.host;
    pmtiles = "pmtiles://" + protHost + window.location.pathname + options.pmtiles;
  }
  return {
          container: options.container,
          style: {
            version: 8,
            glyphs: options.fontsRoot + "/{fontstack}/{range}.pbf",
            sprite: options.spriteRoot + "/" + options.theme,
            sources: {
              protomaps: {
                type: "vector",
                url: pmtiles,
                attribution: options.attributions.join(" | ")
              }
            }
          },
          center: options.center,
          zoom: options.zoom
        };
}

var GeoMapOptions = {
  t_encode: t_encode$1,
  t_decode: t_decode$1,
  make: make
};

function makeMarker(options) {
  var marker = document.createElement("div");
  marker.setAttribute("class", "geo-marker");
  var geoIcon = document.createElement("div");
  geoIcon.setAttribute("class", "geo-icon");
  var geoInfo = document.createElement("div");
  geoInfo.setAttribute("class", "geo-info pop-box");
  var popInfo = document.createElement("div");
  popInfo.setAttribute("class", "pop-info");
  marker.appendChild(geoIcon);
  marker.appendChild(geoInfo);
  geoInfo.appendChild(popInfo);
  var show = options.dir;
  if (show !== undefined && show) {
    var popDir = document.createElement("i");
    popDir.setAttribute("class", "pop-dir top pos-top-left");
    geoInfo.appendChild(popDir);
  }
  var caption = options.caption;
  var markerCaption;
  switch (caption.TAG) {
    case "MarkerCaption" :
        var title = document.createElement("div");
        title.setAttribute("class", "geo-icon-caption");
        title.innerHTML = caption._0;
        geoIcon.appendChild(title);
        markerCaption = title;
        break;
    case "MarkerIcon" :
        var icon = document.createElement("i");
        icon.setAttribute("class", "geo-icon-icon nf " + caption._0);
        icon.setAttribute("style", "color:" + caption._1);
        icon.setAttribute("title", caption._2);
        geoIcon.appendChild(icon);
        markerCaption = icon;
        break;
    case "MarkerImage" :
        var title$1 = caption._1;
        var img = document.createElement("img");
        img.setAttribute("class", "geo-icon-image");
        img.setAttribute("alt", title$1);
        img.setAttribute("title", title$1);
        img.setAttribute("src", caption._0);
        geoIcon.appendChild(img);
        markerCaption = img;
        break;
    
  }
  var sub = options.subCaption;
  if (sub !== undefined) {
    var subElt = document.createElement("div");
    subElt.setAttribute("class", "geo-icon-subcaption");
    subElt.innerHTML = sub;
    geoIcon.appendChild(subElt);
  }
  popInfo.innerHTML = options.text;
  var dset = function (v, top, left) {
    return "display:" + v + ";position:absolute;top:" + top.toString() + "px;left:" + left.toString() + "px";
  };
  var showHide = function (elt, type_, x, y) {
    var hover = function (elt, display, action) {
      var s = elt.getAttribute("data-status");
      if (!(s == null)) {
        if (s === "clicked") {
          if (action === "click") {
            elt.setAttribute("data-status", "");
            elt.setAttribute("style", dset("none", x, y));
            return ;
          } else {
            return ;
          }
        } else {
          if (action === "click") {
            elt.setAttribute("data-status", "clicked");
            elt.setAttribute("style", dset(display, x, y));
          } else {
            elt.setAttribute("data-status", "");
            elt.setAttribute("style", dset(display, x, y));
          }
          return ;
        }
      }
      elt.setAttribute("data-status", "");
      elt.setAttribute("style", dset(display, x, y));
    };
    switch (type_) {
      case "mousedown" :
          return hover(elt, "block", "click");
      case "mouseenter" :
          return hover(elt, "block", "");
      case "mouseout" :
          return hover(elt, "none", "");
      default:
        elt.setAttribute("data-status", "");
        elt.setAttribute("style", "display:none");
        return ;
    }
  };
  var popup = function (ev) {
    ev.stopImmediatePropagation();
    var typ = ev.type;
    var target = ev.target;
    var parent = target.parentElement;
    if (parent == null) {
      return ;
    }
    var targetSibling = parent.nextElementSibling;
    if (targetSibling == null) {
      return ;
    }
    var bounds = parent.getBoundingClientRect();
    var ptop = bounds.height | 0;
    var dir = Array.prototype.slice.call(targetSibling.getElementsByClassName("pos-top-left"))[0];
    if (dir === undefined) {
      return showHide(targetSibling, typ, ptop + 4 | 0, -(ptop / 2 | 0) | 0);
    }
    var cstyle = window.getComputedStyle(Caml_option.valFromOption(dir));
    var left = Core__Option.getOr(Core__Int.fromString(cstyle.left, undefined), 0);
    showHide(targetSibling, typ, (ptop << 1) + 4 | 0, (-left | 0) - (ptop / 2 | 0) | 0);
  };
  markerCaption.addEventListener("mouseout", popup);
  markerCaption.addEventListener("mouseenter", popup);
  geoIcon.addEventListener("mousedown", popup);
  return marker;
}

var MakeMapOptions = {};

var MakeMarkerOptions = {};

function addMarker(map, markerOptions) {
  var markerEl = makeMarker(markerOptions);
  return new MaplibreGl.Marker({
                  element: markerEl
                }).setLngLat(markerOptions.coord).addTo(map);
}

function addMarkers(map, markers) {
  return Core__Array.reduce(markers, [], (function (acc, m) {
                acc.push(addMarker(map, m));
                return acc;
              }));
}

function addMarkersJson(map, json) {
  var markers = markers_decode(json);
  if (markers.TAG === "Ok") {
    return addMarkers(map, markers._0);
  } else {
    return [];
  }
}

function makeMapStruct(options) {
  var mapOpts = make(options);
  var newrecord = Caml_obj.obj_dup(mapOpts.style);
  newrecord.layers = ProtomapsThemesBase("protomaps", options.theme);
  var newrecord$1 = Caml_obj.obj_dup(mapOpts);
  newrecord$1.style = newrecord;
  var map = MapGl.make(newrecord$1);
  map.on("load", (function () {
          var myBounds = map.getSource("protomaps").bounds;
          map.setMaxBounds(myBounds);
        }));
  return map;
}

function makeMapJson(json) {
  var opts = t_decode$1(json);
  if (opts.TAG === "Ok") {
    return makeMapStruct(opts._0);
  }
  
}

function makeMap(options) {
  if (options.TAG === "JSON") {
    return makeMapJson(options._0);
  } else {
    return makeMapStruct(options._0);
  }
}

function makeMapWithMarkersJson(options, markers) {
  var map = makeMapJson(options);
  if (map !== undefined) {
    return [
            map,
            addMarkersJson(map, markers)
          ];
  } else {
    return [
            undefined,
            []
          ];
  }
}

function makeMapWithMarkers(options, markers) {
  if (options.TAG === "JSON") {
    var mapOpts = options._0;
    if (markers.TAG === "JSON") {
      return makeMapWithMarkersJson(mapOpts, markers._0);
    }
    var map = makeMapJson(mapOpts);
    if (map !== undefined) {
      return [
              map,
              addMarkers(map, markers._0)
            ];
    } else {
      return [
              undefined,
              []
            ];
    }
  }
  var mapOpts$1 = options._0;
  if (markers.TAG !== "JSON") {
    var markers$1 = markers._0;
    var map$1 = makeMapStruct(mapOpts$1);
    if (map$1 !== undefined) {
      return [
              map$1,
              addMarkers(map$1, markers$1)
            ];
    } else {
      return [
              undefined,
              []
            ];
    }
  }
  var map$2 = makeMapStruct(mapOpts$1);
  if (map$2 !== undefined) {
    return [
            map$2,
            addMarkersJson(map$2, markers._0)
          ];
  } else {
    return [
            undefined,
            []
          ];
  }
}

export {
  markerCaption_encode ,
  markerCaption_decode ,
  MarkerOptions ,
  Net ,
  GeoMapOptions ,
  makeMarker ,
  MakeMapOptions ,
  MakeMarkerOptions ,
  addMarker ,
  addMarkers ,
  addMarkersJson ,
  makeMapStruct ,
  makeMapJson ,
  makeMap ,
  makeMapWithMarkersJson ,
  makeMapWithMarkers ,
}
/* MapGl Not a pure module */
