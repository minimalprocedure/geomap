// Generated by ReScript, PLEASE EDIT WITH CARE

import * as Spice from "@greenlabs/ppx-spice/src/rescript/Spice.res.mjs";
import * as LngLat from "./LngLat.res.mjs";
import * as Js_dict from "rescript/lib/es6/js_dict.js";
import * as Pmtiles from "pmtiles";
import * as Belt_Option from "rescript/lib/es6/belt_Option.js";
import * as MaplibreGl from "maplibre-gl";

function layer_encode(v) {
  return Js_dict.fromArray(Spice.filterOptional([
                  [
                    "id",
                    false,
                    Spice.stringToJson(v.id)
                  ],
                  [
                    "type_",
                    false,
                    Spice.stringToJson(v.type)
                  ],
                  [
                    "source",
                    true,
                    (function (extra) {
                          return Spice.optionToJson(Spice.stringToJson, extra);
                        })(v.source)
                  ],
                  [
                    "source_layer",
                    true,
                    (function (extra) {
                          return Spice.optionToJson(Spice.stringToJson, extra);
                        })(v["source-layer"])
                  ],
                  [
                    "minzoom",
                    true,
                    (function (extra) {
                          return Spice.optionToJson(Spice.floatToJson, extra);
                        })(v.minzoom)
                  ],
                  [
                    "maxzoom",
                    true,
                    (function (extra) {
                          return Spice.optionToJson(Spice.floatToJson, extra);
                        })(v.maxzoom)
                  ]
                ]));
}

function layer_decode(v) {
  if (!Array.isArray(v) && (v === null || typeof v !== "object") && typeof v !== "number" && typeof v !== "string" && typeof v !== "boolean") {
    return Spice.error(undefined, "Not an object", v);
  }
  if (!(typeof v === "object" && !Array.isArray(v))) {
    return Spice.error(undefined, "Not an object", v);
  }
  var id = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "id"), null));
  if (id.TAG === "Ok") {
    var type_ = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "type_"), null));
    if (type_.TAG === "Ok") {
      var source = (function (extra) {
            return Spice.optionFromJson(Spice.stringFromJson, extra);
          })(Belt_Option.getWithDefault(Js_dict.get(v, "source"), null));
      if (source.TAG === "Ok") {
        var source_layer = (function (extra) {
              return Spice.optionFromJson(Spice.stringFromJson, extra);
            })(Belt_Option.getWithDefault(Js_dict.get(v, "source_layer"), null));
        if (source_layer.TAG === "Ok") {
          var minzoom = (function (extra) {
                return Spice.optionFromJson(Spice.floatFromJson, extra);
              })(Belt_Option.getWithDefault(Js_dict.get(v, "minzoom"), null));
          if (minzoom.TAG === "Ok") {
            var maxzoom = (function (extra) {
                  return Spice.optionFromJson(Spice.floatFromJson, extra);
                })(Belt_Option.getWithDefault(Js_dict.get(v, "maxzoom"), null));
            if (maxzoom.TAG === "Ok") {
              return {
                      TAG: "Ok",
                      _0: {
                        id: id._0,
                        type: type_._0,
                        source: source._0,
                        "source-layer": source_layer._0,
                        minzoom: minzoom._0,
                        maxzoom: maxzoom._0
                      }
                    };
            }
            var e = maxzoom._0;
            return {
                    TAG: "Error",
                    _0: {
                      path: ".maxzoom" + e.path,
                      message: e.message,
                      value: e.value
                    }
                  };
          }
          var e$1 = minzoom._0;
          return {
                  TAG: "Error",
                  _0: {
                    path: ".minzoom" + e$1.path,
                    message: e$1.message,
                    value: e$1.value
                  }
                };
        }
        var e$2 = source_layer._0;
        return {
                TAG: "Error",
                _0: {
                  path: ".source_layer" + e$2.path,
                  message: e$2.message,
                  value: e$2.value
                }
              };
      }
      var e$3 = source._0;
      return {
              TAG: "Error",
              _0: {
                path: ".source" + e$3.path,
                message: e$3.message,
                value: e$3.value
              }
            };
    }
    var e$4 = type_._0;
    return {
            TAG: "Error",
            _0: {
              path: ".type_" + e$4.path,
              message: e$4.message,
              value: e$4.value
            }
          };
  }
  var e$5 = id._0;
  return {
          TAG: "Error",
          _0: {
            path: ".id" + e$5.path,
            message: e$5.message,
            value: e$5.value
          }
        };
}

function layers_encode(v) {
  return Spice.arrayToJson(layer_encode, v);
}

function layers_decode(v) {
  return Spice.arrayFromJson(layer_decode, v);
}

function bounds_encode(v) {
  return Spice.listToJson(Spice.floatToJson, v);
}

function bounds_decode(v) {
  return Spice.listFromJson(Spice.floatFromJson, v);
}

function protomaps_encode(v) {
  return Js_dict.fromArray(Spice.filterOptional([
                  [
                    "type_",
                    false,
                    Spice.stringToJson(v.type)
                  ],
                  [
                    "url",
                    false,
                    Spice.stringToJson(v.url)
                  ],
                  [
                    "attribution",
                    false,
                    Spice.stringToJson(v.attribution)
                  ]
                ]));
}

function protomaps_decode(v) {
  if (!Array.isArray(v) && (v === null || typeof v !== "object") && typeof v !== "number" && typeof v !== "string" && typeof v !== "boolean") {
    return Spice.error(undefined, "Not an object", v);
  }
  if (!(typeof v === "object" && !Array.isArray(v))) {
    return Spice.error(undefined, "Not an object", v);
  }
  var type_ = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "type_"), null));
  if (type_.TAG === "Ok") {
    var url = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "url"), null));
    if (url.TAG === "Ok") {
      var attribution = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "attribution"), null));
      if (attribution.TAG === "Ok") {
        return {
                TAG: "Ok",
                _0: {
                  type: type_._0,
                  url: url._0,
                  attribution: attribution._0
                }
              };
      }
      var e = attribution._0;
      return {
              TAG: "Error",
              _0: {
                path: ".attribution" + e.path,
                message: e.message,
                value: e.value
              }
            };
    }
    var e$1 = url._0;
    return {
            TAG: "Error",
            _0: {
              path: ".url" + e$1.path,
              message: e$1.message,
              value: e$1.value
            }
          };
  }
  var e$2 = type_._0;
  return {
          TAG: "Error",
          _0: {
            path: ".type_" + e$2.path,
            message: e$2.message,
            value: e$2.value
          }
        };
}

function sources_encode(v) {
  return Js_dict.fromArray(Spice.filterOptional([[
                    "protomaps",
                    false,
                    protomaps_encode(v.protomaps)
                  ]]));
}

function sources_decode(v) {
  if (!Array.isArray(v) && (v === null || typeof v !== "object") && typeof v !== "number" && typeof v !== "string" && typeof v !== "boolean") {
    return Spice.error(undefined, "Not an object", v);
  }
  if (!(typeof v === "object" && !Array.isArray(v))) {
    return Spice.error(undefined, "Not an object", v);
  }
  var protomaps = protomaps_decode(Belt_Option.getWithDefault(Js_dict.get(v, "protomaps"), null));
  if (protomaps.TAG === "Ok") {
    return {
            TAG: "Ok",
            _0: {
              protomaps: protomaps._0
            }
          };
  }
  var e = protomaps._0;
  return {
          TAG: "Error",
          _0: {
            path: ".protomaps" + e.path,
            message: e.message,
            value: e.value
          }
        };
}

function style_encode(v) {
  return Js_dict.fromArray(Spice.filterOptional([
                  [
                    "version",
                    false,
                    Spice.intToJson(v.version)
                  ],
                  [
                    "glyphs",
                    false,
                    Spice.stringToJson(v.glyphs)
                  ],
                  [
                    "sprite",
                    false,
                    Spice.stringToJson(v.sprite)
                  ],
                  [
                    "sources",
                    false,
                    sources_encode(v.sources)
                  ],
                  [
                    "layers",
                    true,
                    (function (extra) {
                          return Spice.optionToJson(layers_encode, extra);
                        })(v.layers)
                  ]
                ]));
}

function style_decode(v) {
  if (!Array.isArray(v) && (v === null || typeof v !== "object") && typeof v !== "number" && typeof v !== "string" && typeof v !== "boolean") {
    return Spice.error(undefined, "Not an object", v);
  }
  if (!(typeof v === "object" && !Array.isArray(v))) {
    return Spice.error(undefined, "Not an object", v);
  }
  var version = Spice.intFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "version"), null));
  if (version.TAG === "Ok") {
    var glyphs = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "glyphs"), null));
    if (glyphs.TAG === "Ok") {
      var sprite = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "sprite"), null));
      if (sprite.TAG === "Ok") {
        var sources = sources_decode(Belt_Option.getWithDefault(Js_dict.get(v, "sources"), null));
        if (sources.TAG === "Ok") {
          var layers = (function (extra) {
                return Spice.optionFromJson(layers_decode, extra);
              })(Belt_Option.getWithDefault(Js_dict.get(v, "layers"), null));
          if (layers.TAG === "Ok") {
            return {
                    TAG: "Ok",
                    _0: {
                      version: version._0,
                      glyphs: glyphs._0,
                      sprite: sprite._0,
                      sources: sources._0,
                      layers: layers._0
                    }
                  };
          }
          var e = layers._0;
          return {
                  TAG: "Error",
                  _0: {
                    path: ".layers" + e.path,
                    message: e.message,
                    value: e.value
                  }
                };
        }
        var e$1 = sources._0;
        return {
                TAG: "Error",
                _0: {
                  path: ".sources" + e$1.path,
                  message: e$1.message,
                  value: e$1.value
                }
              };
      }
      var e$2 = sprite._0;
      return {
              TAG: "Error",
              _0: {
                path: ".sprite" + e$2.path,
                message: e$2.message,
                value: e$2.value
              }
            };
    }
    var e$3 = glyphs._0;
    return {
            TAG: "Error",
            _0: {
              path: ".glyphs" + e$3.path,
              message: e$3.message,
              value: e$3.value
            }
          };
  }
  var e$4 = version._0;
  return {
          TAG: "Error",
          _0: {
            path: ".version" + e$4.path,
            message: e$4.message,
            value: e$4.value
          }
        };
}

function mapOption_encode(v) {
  return Js_dict.fromArray(Spice.filterOptional([
                  [
                    "container",
                    false,
                    Spice.stringToJson(v.container)
                  ],
                  [
                    "style",
                    false,
                    style_encode(v.style)
                  ],
                  [
                    "center",
                    true,
                    (function (extra) {
                          return Spice.optionToJson(LngLat.lngLat_encode, extra);
                        })(v.center)
                  ],
                  [
                    "bounds",
                    true,
                    (function (extra) {
                          return Spice.optionToJson(bounds_encode, extra);
                        })(v.bounds)
                  ],
                  [
                    "zoom",
                    true,
                    (function (extra) {
                          return Spice.optionToJson(Spice.floatToJson, extra);
                        })(v.zoom)
                  ]
                ]));
}

function mapOption_decode(v) {
  if (!Array.isArray(v) && (v === null || typeof v !== "object") && typeof v !== "number" && typeof v !== "string" && typeof v !== "boolean") {
    return Spice.error(undefined, "Not an object", v);
  }
  if (!(typeof v === "object" && !Array.isArray(v))) {
    return Spice.error(undefined, "Not an object", v);
  }
  var container = Spice.stringFromJson(Belt_Option.getWithDefault(Js_dict.get(v, "container"), null));
  if (container.TAG === "Ok") {
    var style = style_decode(Belt_Option.getWithDefault(Js_dict.get(v, "style"), null));
    if (style.TAG === "Ok") {
      var center = (function (extra) {
            return Spice.optionFromJson(LngLat.lngLat_decode, extra);
          })(Belt_Option.getWithDefault(Js_dict.get(v, "center"), null));
      if (center.TAG === "Ok") {
        var bounds = (function (extra) {
              return Spice.optionFromJson(bounds_decode, extra);
            })(Belt_Option.getWithDefault(Js_dict.get(v, "bounds"), null));
        if (bounds.TAG === "Ok") {
          var zoom = (function (extra) {
                return Spice.optionFromJson(Spice.floatFromJson, extra);
              })(Belt_Option.getWithDefault(Js_dict.get(v, "zoom"), null));
          if (zoom.TAG === "Ok") {
            return {
                    TAG: "Ok",
                    _0: {
                      container: container._0,
                      style: style._0,
                      center: center._0,
                      bounds: bounds._0,
                      zoom: zoom._0
                    }
                  };
          }
          var e = zoom._0;
          return {
                  TAG: "Error",
                  _0: {
                    path: ".zoom" + e.path,
                    message: e.message,
                    value: e.value
                  }
                };
        }
        var e$1 = bounds._0;
        return {
                TAG: "Error",
                _0: {
                  path: ".bounds" + e$1.path,
                  message: e$1.message,
                  value: e$1.value
                }
              };
      }
      var e$2 = center._0;
      return {
              TAG: "Error",
              _0: {
                path: ".center" + e$2.path,
                message: e$2.message,
                value: e$2.value
              }
            };
    }
    var e$3 = style._0;
    return {
            TAG: "Error",
            _0: {
              path: ".style" + e$3.path,
              message: e$3.message,
              value: e$3.value
            }
          };
  }
  var e$4 = container._0;
  return {
          TAG: "Error",
          _0: {
            path: ".container" + e$4.path,
            message: e$4.message,
            value: e$4.value
          }
        };
}

var Source = {};

function init() {
  var protocol = new Pmtiles.Protocol();
  MaplibreGl.addProtocol("pmtiles", protocol.tile);
}

var Protocol = {
  init: init
};

function make(mapOption) {
  var map = new MaplibreGl.Map(mapOption);
  init();
  return map;
}

export {
  layer_encode ,
  layer_decode ,
  layers_encode ,
  layers_decode ,
  bounds_encode ,
  bounds_decode ,
  protomaps_encode ,
  protomaps_decode ,
  sources_encode ,
  sources_decode ,
  style_encode ,
  style_decode ,
  mapOption_encode ,
  mapOption_decode ,
  Source ,
  Protocol ,
  make ,
}
/* pmtiles Not a pure module */
