/* Copyright (c) 2024 Massimo Ghisalberti
*
*  This software is released under the MIT License.
*  https://opensource.org/licenses/MIT 
*/

type rec marker<'a> = {
  setLngLat: LngLat.lngLat => marker<'a>,
  addTo: MapGl.mapGl => marker<'a>,
}

type markerOptions = {
  color?: string,
  draggable?: bool,
  className?: string,
  scale?: float,
  element: Dom.element,
}

@module("maplibre-gl") @new external make: markerOptions => marker<'a> = "Marker"
