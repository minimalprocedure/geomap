/* Copyright (c) 2024 Massimo Ghisalberti
*
*  This software is released under the MIT License.
*  https://opensource.org/licenses/MIT 
*/

let protocol = Utils.Net.current_protocol
let host = Utils.Net.current_host
let pathname = Utils.Net.current_pathname

let palazzoCoord = [
  LngLat.degToDec(12.0, 34.0, 12.0, LngLat.Cardinal.E),
  LngLat.degToDec(43.0, 5.0, 46.68, LngLat.Cardinal.N),
]
let perugiaCoord = [
  LngLat.degToDec(12.0, 23.0, 19.68, LngLat.Cardinal.E),
  LngLat.degToDec(43.0, 6.0, 43.56, LngLat.Cardinal.N),
]
/*
let options: GeoMap.GeoMapOptions.t = {
  container: "map",
  center: perugiaCoord,
  zoom: 10.0,
  fontsRoot: "fonts",
  spriteRoot: "sprites",
  localPmtiles: "hmb",
  attributions: [
    `<a href="https://protomaps.com">Protomaps</a>`,
    `© <a href="https://openstreetmap.org">OpenStreetMap</a>`,
  ],
  theme: "light",
}
*/

let mapJSON = %raw(`
{
  "container": "map",
  "center": [ 12.3888, 43.1121 ],
  "zoom": 10.0,
  "fontsRoot": "fonts",
  "spriteRoot": "sprites",
  "pmtiles": "hmb.pmtiles",
  "attributions": [
    "<a href=\"https://protomaps.com\">Protomaps</a>",
    "© <a href=\"https://openstreetmap.org\">OpenStreetMap</a>",
  ],
  "theme": "light",
}
`)

let markersJSON = %raw(`
[{
    "caption": ["MarkerImage", "/sprites/v3/light@2x.png", "Palazzo d'Assisi"],
    "subCaption": "Palazzo d'Assisi",
    "text": "scava qui per il tesoro! Prova!",
    "coord": [ 12.57, 43.0963 ]
  },
  {
    "caption": ["MarkerIcon", "nf-fa-location_pin", "#ff0000", "Perugia"],
    "text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam venenatis\
      porttitor pellentesque. Suspendisse quam leo, gravida ut ligula nec,\
      semper mollis eros. Aliquam interdum tellus sit amet luctus interdum.\
      Vestibulum velit mauris, facilisis elementum elit et, molestie dictum velit.\
      Etiam consequat tempus nisi ut faucibus. Donec varius in erat et eleifend.\
      Duis aliquet elementum odio, eget rhoncus sapien interdum ut.",
    "coord": [ 12.3888, 43.1121 ]
  }]
`)

let (_,_) = GeoMap.makeMapWithMarkersJson(mapJSON, markersJSON)
/*
let map = GeoMap.makeMapJson(mapJSON)

switch map {
| None => ()
| Some(map) => {
    let _ = GeoMap.addMarkersJson(map, markersJSON)
  }
}
*/
/*
let markers1 = GeoMap.MarkerOptions.markers_decode(markersJSON)

Console.log(markers1)

let markers: GeoMap.MarkerOptions.markers = [
  {
    caption: GeoMap.MarkerImage("/sprites/v3/light@2x.png", "Palazzo d'Assisi"),
    subCaption: Some("Palazzo d'Assisi"),
    text: "scava qui per il tesoro!",
    coord: palazzoCoord,
  },
  {
    caption: GeoMap.MarkerIcon("nf-fa-location_pin", "#ff0000", "Perugia"),
    subCaption: Some(""),
    text: `    
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam venenatis 
      porttitor pellentesque. Suspendisse quam leo, gravida ut ligula nec, 
      semper mollis eros. Aliquam interdum tellus sit amet luctus interdum. 
      Vestibulum velit mauris, facilisis elementum elit et, molestie dictum velit. 
      Etiam consequat tempus nisi ut faucibus. Donec varius in erat et eleifend. 
      Duis aliquet elementum odio, eget rhoncus sapien interdum ut. 
    `,
    coord: perugiaCoord,
  },
]

switch markers1 {
| Error(m) => Console.log(m)
| Ok(markers) => Array.forEach(markers, m => {
    let _ = GeoMap.addMarkerTo(map, m)
  })
}

/*
let markerInfo = GeoMap.addMarkerTo(
  map,
  {
    caption: GeoMap.MarkerImage("/sprites/v3/light@2x.png", "Palazzo d'Assisi"),
    subCaption: Some("Palazzo d'Assisi"),
    text: "scava qui per il tesoro!",
    coord: palazzoCoord,
  },
  ~draggable=true,
)
*/

map.on("load", () => {
  let myBounds = map.getSource("protomaps").bounds
  map.setMaxBounds(myBounds)
})

*/
