/* Copyright (c) 2024 Massimo Ghisalberti
*
*  This software is released under the MIT License.
*  https://opensource.org/licenses/MIT 
*/

module Net = {
  @val @scope(("window", "location")) external current_protocol: string = "protocol"
  @val @scope(("window", "location")) external current_host: string = "host"
  @val @scope(("window", "location")) external current_pathname: string = "pathname"
}
